﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using XOProject.Repository.Exchange;
using XOProject.Services.Domain;

namespace XOProject.Services.Exchange
{
    public class AnalyticsService : IAnalyticsService
    {
        private readonly IShareRepository _shareRepository;

        public AnalyticsService(IShareRepository shareRepository)
        {
            _shareRepository = shareRepository;
        }

        public async Task<AnalyticsPrice> GetDailyAsync(string symbol, DateTime day)
        {
            var rates = await _shareRepository
                               .Query()
                               .Where(x => x.Symbol.Equals(symbol)
                                           && x.TimeStamp.Date.Date == day.Date)
                               .OrderBy(x => x.TimeStamp)
                               .ToListAsync();


            if (!rates.Any())
            {
                return null;
            }

            return new AnalyticsPrice()
            {
                Open = rates.First().Rate,
                High = rates.Max(x => x.Rate),
                Low = rates.Min(x => x.Rate),
                Close = rates.Last().Rate
            };
        }

        private DateTime FirstDayOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime firstMonth = new DateTime(year, 1, 1);
            int daysOff = DayOfWeek.Thursday - firstMonth.DayOfWeek;

            DateTime firstThursday = firstMonth.AddDays(daysOff);
            var calendar = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = calendar.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var week = weekOfYear;
            if (firstWeek == 1)
            {
                week -= 1;
            }

            var result = firstThursday.AddDays(week * 7);

            return result.AddDays(-3);
        }

        public async Task<AnalyticsPrice> GetWeeklyAsync(string symbol, int year, int week)
        {
            if (year <= 0 || week <= 0 || week > 53)
            {
                return null;
            }

            DateTime firstDayOfWeek = FirstDayOfWeekISO8601(year, week);
            DateTime lastDayOfWeek = firstDayOfWeek.AddDays(6);

            var rates = await _shareRepository
                               .Query()
                               .Where(x => x.Symbol.Equals(symbol)
                                           && x.TimeStamp.Date >= firstDayOfWeek
                                           && x.TimeStamp.Date <= lastDayOfWeek)
                               .OrderBy(x => x.TimeStamp)
                               .ToListAsync();


            if (!rates.Any())
            {
                return null;
            }

            return new AnalyticsPrice()
            {
                Open = rates.First().Rate,
                High = rates.Max(x => x.Rate),
                Low = rates.Min(x => x.Rate),
                Close = rates.Last().Rate
            };
        }

        public async Task<AnalyticsPrice> GetMonthlyAsync(string symbol, int year, int month)
        {
            if (year <= 0 || month <= 0 || month > 12)
            {
                return null;
            }

            DateTime firstDayOfMonth = new DateTime(year, month, 1);
            DateTime lastDayOfMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            var rates = await _shareRepository
                               .Query()
                               .Where(x => x.Symbol.Equals(symbol)
                                           && x.TimeStamp.Date >= firstDayOfMonth
                                           && x.TimeStamp.Date <= lastDayOfMonth)
                               .OrderBy(x => x.TimeStamp)
                               .ToListAsync();

            if (!rates.Any())
            {
                return null;
            }

            return new AnalyticsPrice()
            {
                Open = rates.First().Rate,
                High = rates.Max(x => x.Rate),
                Low = rates.Min(x => x.Rate),
                Close = rates.Last().Rate
            };
        }
    }
}