﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Repository.Tests.Helpers;

namespace XOProject.Repository.Tests
{
    public class PortfolioRepositoryTests
    {
        private PortfolioRepository _portfolioRepository;
        private ExchangeContext _context;

        [SetUp]
        public void Initialize()
        {
            _context = ContextFactory.CreateContext(true);
            _portfolioRepository = new PortfolioRepository(_context);
        }

        [TearDown]
        public void Cleanup()
        {
            _context.Dispose();
            _context = null;
            _portfolioRepository = null;
        }

        [Test]
        public async Task GetAsync_WhenExists_ReturnsPortfolio()
        {
            // Arrange
            var expected = new Portfolio
                { Id = 1, Name = "John Doe" };


            // Act
            var result = await _portfolioRepository.GetAsync(1);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Name, result.Name);
            Assert.AreEqual(expected.Trades, result.Trades);
        }

        [Test]
        public async Task GetAsync_WhenDoesNotExist_ReturnsNull()
        {
            // Arrange
            
            // Act
            var result = await _portfolioRepository.GetAsync(99);

            // Assert
            Assert.IsNull(result);
        }
    }
}
