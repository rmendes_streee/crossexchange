﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Repository.Tests.Helpers;

namespace XOProject.Repository.Tests
{
    public class TradeRepositoryTests
    {
        private TradeRepository _tradeRepository;
        private ExchangeContext _context;

        [SetUp]
        public void Initialize()
        {
            _context = ContextFactory.CreateContext(true);
            _tradeRepository = new TradeRepository(_context);
        }

        [TearDown]
        public void Cleanup()
        {
            _context.Dispose();
            _context = null;
            _tradeRepository = null;
        }

        [Test]
        public async Task GetAsync_WhenExists_ReturnsTrades()
        {
            // Arrange
            var expected = new Trade
            { Id = 1, Action = "BUY", Symbol = "REL", ContractPrice = 5000.0M, NoOfShares = 50, PortfolioId = 1 };


            // Act
            var result = await _tradeRepository.GetAsync(1);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Action, result.Action);
            Assert.AreEqual(expected.Symbol, result.Symbol);
            Assert.AreEqual(expected.ContractPrice, result.ContractPrice);
            Assert.AreEqual(expected.NoOfShares, result.NoOfShares);
            Assert.AreEqual(expected.PortfolioId, result.PortfolioId);
        }

        [Test]
        public async Task GetAsync_WhenDoesNotExist_ReturnsNull()
        {
            // Arrange
            
            // Act
            var result = await _tradeRepository.GetAsync(99);

            // Assert
            Assert.IsNull(result);
        }
    }
}
