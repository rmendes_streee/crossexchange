﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Services.Exchange;
using XOProject.Services.Tests.Helpers;
using System.Linq;

namespace XOProject.Services.Tests
{
    public class TradeServiceTests
    {
        private readonly Mock<IShareRepository> _shareRepositoryMock = new Mock<IShareRepository>();
        private readonly Mock<ITradeRepository> _tradeRepositoryMock = new Mock<ITradeRepository>();

        private readonly TradeService _tradeService;
        private readonly ShareService _shareService;

        public TradeServiceTests()
        {
            _shareService = new ShareService(_shareRepositoryMock.Object);
            _tradeService = new TradeService(_tradeRepositoryMock.Object, _shareService);
        }

        [TearDown]
        public void Cleanup()
        {
            _shareRepositoryMock.Reset();
            _tradeRepositoryMock.Reset();
        }

        [Test]
        public async Task GetHourlyAsync_WhenExists_GetsTrades()
        {
            // Arrange
            ArrangeTrades();

            // Act
            var result = await _tradeService.GetAllAsync();

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].Id);
            Assert.AreEqual("REL", result[0].Symbol);
            Assert.AreEqual(2, result[1].Id);
            Assert.AreEqual("CBI", result[1].Symbol);
        }

        private void ArrangeTrades()
        {
            var trades = new[]
            {
                new Trade()
                {
                    Id = 1,
                    Symbol = "REL",
                    NoOfShares = 20,
                    ContractPrice = 5000.0M,
                    PortfolioId = 1,
                    Action = "BUY"
                },
                new Trade()
                {
                    Id = 2,
                    Symbol = "CBI",
                    NoOfShares = 10,
                    ContractPrice = 1500.0M,
                    PortfolioId = 1,
                    Action = "BUY"
                }
            };

            _tradeRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<Trade>(trades));
        }
    }
}
