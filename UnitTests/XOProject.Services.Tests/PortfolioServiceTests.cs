﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Services.Exchange;
using XOProject.Services.Tests.Helpers;
using System.Linq;

namespace XOProject.Services.Tests
{
    public class PortfolioServiceTests
    {
        private readonly Mock<IPortfolioRepository> _portfolioRepositoryMock = new Mock<IPortfolioRepository>();

        private readonly PortfolioService _portfolioService;

        public PortfolioServiceTests()
        {
            _portfolioService = new PortfolioService(_portfolioRepositoryMock.Object);
        }

        [TearDown]
        public void Cleanup()
        {
            _portfolioRepositoryMock.Reset();
        }

        [Test]
        public async Task GetHourlyAsync_WhenExists_GetsPortfolio()
        {
            // Arrange
            ArrangePortfolio();

            // Act
            var result = await _portfolioService.GetAllAsync();

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].Id);
            Assert.AreEqual("John Doe", result[0].Name);
            Assert.AreEqual(2, result[1].Id);
            Assert.AreEqual("Isis Kane", result[1].Name);
        }

        private void ArrangePortfolio()
        {
            var portfolio = new[]
            {
                new Portfolio()
                {
                    Id = 1,
                    Name = "John Doe"
                },
                new Portfolio()
                {
                    Id = 2,
                    Name = "Isis Kane"
                }
            };

            _portfolioRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<Portfolio>(portfolio));
        }
    }
}
