﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using XOProject.Api.Model.Analytics;
using XOProject.Services.Domain;
using XOProject.Services.Exchange;

namespace XOProject.Api.Controller
{
    [Route("api")]
    public class AnalyticsController : ControllerBase
    {
        private readonly IAnalyticsService _analyticsService;

        public AnalyticsController(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;
        }

        [HttpGet("daily/{symbol}/{year}/{month}/{day}")]
        public async Task<IActionResult> Daily([FromRoute] string symbol, [FromRoute] int year, [FromRoute] int month, [FromRoute] int day)
        {
            DateTime date = new DateTime(year, month, day);
            var daySummary = await _analyticsService.GetDailyAsync(symbol, date);
            
            if (daySummary == null)
            {
                return NotFound();
            }

            var result = new DailyModel()
            {
                Symbol = symbol,
                Day = date,
                Price = Map(daySummary)
            };

            return Ok(result);
        }

        [HttpGet("weekly/{symbol}/{year}/{week}")]
        public async Task<IActionResult> Weekly([FromRoute] string symbol, [FromRoute] int year, [FromRoute] int week)
        {
            var weeklySummary = await _analyticsService.GetWeeklyAsync(symbol, year, week);

            if (weeklySummary == null)
            {
                return NotFound();
            }

            var result = new WeeklyModel()
            {
                Symbol = symbol,
                Year = year,
                Week = week,
                Price = Map(weeklySummary)
            };

            return Ok(result);
        }

        [HttpGet("monthly/{symbol}/{year}/{month}")]
        public async Task<IActionResult> Monthly([FromRoute] string symbol, [FromRoute] int year, [FromRoute] int month)
        {
            var monthlySummary = await _analyticsService.GetMonthlyAsync(symbol, year, month);

            if (monthlySummary == null)
            {
                return NotFound();
            }

            var result = new MonthlyModel()
            {
                Symbol = symbol,
                Year = year,
                Month = month,
                Price = Map(monthlySummary)
            };

            return Ok(result);
        }

        private PriceModel Map(AnalyticsPrice price)
        {
            return new PriceModel()
            {
                Open = price.Open,
                Close = price.Close,
                High = price.High,
                Low = price.Low
            };
        }
    }
}