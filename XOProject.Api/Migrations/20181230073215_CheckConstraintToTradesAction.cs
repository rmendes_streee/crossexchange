﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XOProject.Api.Migrations
{
    public partial class CheckConstraintToTradesAction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("ALTER TABLE Trades " +
               "ADD CONSTRAINT CHK_Action_Val CHECK(Action in ('BUY', 'SELL'))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
