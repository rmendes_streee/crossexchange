﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XOProject.Api.Migrations
{
    public partial class UniqueConstraintToShares : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Symbol",
                table: "Shares",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddUniqueConstraint(
                name: "UK_HourlyRates",
                table: "Shares",
                columns: new[] { "TimeStamp", "Symbol" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "UK_HourlyRates",
                table: "Shares");

            migrationBuilder.AlterColumn<string>(
                name: "Symbol",
                table: "Shares",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
